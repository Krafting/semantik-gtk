# How to use and import custom model and wordlists

## Model File

The model file format is the original binary format proposed by [word2vec](https://code.google.com/archive/p/word2vec/)

Here are a couple of exemple of language file that can be used by SemantiK

[French](https://fauconnier.github.io/#data)

[Multiple Languages](https://github.com/Kyubyong/wordvectors)

[You can find more language here](https://wikipedia2vec.github.io/wikipedia2vec/pretrained/)

## Word-Lists

The word list need to be a text file with one word per line.

You can find plenty of word lists online.

## Import the files

You'll need to put both files in `~/.var/app/net.krafting.SemantiK/config/SemantiK/`

Then in the Preferences window in the Advanced Menu, enter the name (with the extension) of your files.

You also need to select the "Custom" language in the main Preferences window.

You might need to restart the application and generate a new word for settings to take effects.