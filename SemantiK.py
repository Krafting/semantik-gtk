#!/usr/bin/python3
import gi
import sys
import os
import logging as log

gi.require_version("Gtk", "4.0")
gi.require_version("Pango", "1.0")
gi.require_version("Adw", "1")
from gi.repository import Gtk, Adw, Gio, Gdk, GLib, Pango

# Set up the logger
logger = log.getLogger()
logger.setLevel(log.INFO)
formatter = log.Formatter("%(asctime)s | %(levelname)s | %(message)s", "%d-%m-%Y %H:%M:%S")
# Log inside the terminal
stdout_handler = log.StreamHandler(sys.stdout)
stdout_handler.setFormatter(formatter)
logger.addHandler(stdout_handler)

# Utils
from utils.game import *
from utils.utilities import *
from utils.custom_css import *
from utils.translation import *
from utils.keyboard import *

# UI
from utils.ui.header import *
from utils.ui.toast import *
from utils.ui.sharing import *
from utils.ui.dialogs import *
from utils.ui.shortcuts import *
from utils.ui.how_to_play import *
from utils.ui.indice import *
from utils.ui.settings import *
from utils.ui.dialogs import *
from utils.ui.about import *

config_folder = f"{GLib.get_user_config_dir()}/SemantiK"
if not os.path.exists(config_folder):
    log.info("Creating config folder...")
    os.makedirs(config_folder)

class SemantiK(Adw.Application):
    def __init__(self):
        super().__init__(application_id="net.krafting.SemantiK")
        self.connect("activate", self.on_activate)

    def on_activate(self, app):
        # Initialize some variables for use later
        self.available_language = list_dir("/app/share/extra/")
        self.config = {}
        if len(self.available_language) > 0:
            self.current_language = self.available_language[0]
            self.config["set_langue_name"] = self.available_language[0]
        self.all_guesses_variables = {}
        self.all_guesses_order = []
        self.latest_game = {
            "guessed_word": [],
            "guessed_word_unordered": []
        }
        self.animation_timed = ""
        self.is_loading_setting_ui = False
        self.current_history_pos = 0
        self.config_folder = f"{GLib.get_user_config_dir()}/SemantiK"
        self.default_language = Pango.Language.get_default().to_string().lower()

        # Load settings, translation and CSS
        _ = load_translation(self)
        load_settings(just_load_value=True, self=self)
        load_custom_css()

        # Set default settings
        self.window = Adw.ApplicationWindow(application=app)
        self.window.set_default_size(850, 700)
        self.window.set_property("height-request", 200)
        self.window.set_property("width-request", 300)
        self.window.set_title("SemantiK")
        GLib.set_application_name("SemantiK")
        self.set_application_id("net.krafting.SemantiK")
        self.window.present()

        #######################
        # MAIN PAGE
        #######################
        self.Main_boxMain = Adw.ToolbarView()
        # All page is a Toast Overlay to display burnt toast
        self.Main_Toast = Adw.ToastOverlay()
        self.Main_Toast.set_child(self.Main_boxMain)
        self.window.set_content(self.Main_Toast)  # Horizontal box to window

        self.Main_page = Adw.StatusPage()
        self.Main_page.set_title(_("SemantiK"))
        self.Main_page.set_description(_("Trouver le mot secret."))

        # Page qui s'affiche quand on charge une nouvelle langue, le temps de charger les trucs importants.
        self.spinnerLoading = Adw.Spinner()
        self.spinnerLoading.set_property('width-request', 100)
        self.spinnerLoading.set_property('height-request', 100)
        
        self.Main_pageLoading = Adw.StatusPage()
        self.Main_pageLoading.set_title(_("Chargement en cours"))
        self.Main_pageLoading.set_description(_("Merci de patienter..."))
        self.Main_pageLoading.set_child(self.spinnerLoading)

        self.Main_clamp = Adw.Clamp()
        self.Main_clamp.set_property("valign", Gtk.Align.CENTER)
        self.Main_page.set_child(self.Main_clamp)

        self.Main_box2 = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        self.Main_clamp.set_child(self.Main_box2)

        # Page shown when no language packs are installed
        storeButtonContent: Adw.ButtonContent = Adw.ButtonContent(label=_('Ouvrir la logithèque'), icon_name='system-software-install-symbolic')
        storeButton: Gtk.Button = Gtk.Button(child=storeButtonContent, halign=Gtk.Align.CENTER)
        storeButton.connect("clicked", open_store, None, self)
        storeButton.add_css_class('pill')
        storeButton.add_css_class('suggested-action')
        self.noLangError = Adw.StatusPage(title=_('Aucun paquet de langues installé'), description=_("Merci d'en installer d'abord depuis la logithèque"), icon_name='computer-fail-symbolic', child=storeButton)


        # Box with the Success Word Found Action row
        self.Main_listboxFoundWord = Gtk.ListBox()
        self.Main_listboxFoundWord.set_selection_mode(Gtk.SelectionMode.NONE)
        self.Main_listboxFoundWord.add_css_class("boxed-list")
        self.Main_listboxFoundWord.set_visible(False)
        self.Main_listboxFoundWord.add_css_class("success-action-row")
        self.Main_box2.append(self.Main_listboxFoundWord)

        self.Main_Button_FoundWord = Gtk.Button(label=_("Nouveau Mot"))
        self.Main_Button_FoundWord.set_valign(Gtk.Align.CENTER)
        self.Main_Button_FoundWord.add_css_class("success-action-row-button")
        self.Main_Button_FoundWord.connect("clicked", new_word, False, self)

        self.Main_felicitationsActionRow = Adw.ActionRow()
        self.Main_felicitationsActionRow.set_title(_("Félicitations !"))
        self.Main_felicitationsActionRow.add_suffix(self.Main_Button_FoundWord)
        self.Main_felicitationsActionRow.set_subtitle("Vous avez trouvé le mot en XXX coups.")
        self.Main_listboxFoundWord.append(self.Main_felicitationsActionRow)

        self.Main_listbox = Gtk.ListBox()
        self.Main_listbox.set_selection_mode(Gtk.SelectionMode.NONE)
        self.Main_listbox.add_css_class("boxed-list")
        self.Main_box2.append(self.Main_listbox)

        # Input de l'user
        self.WordGuess_Input = Adw.EntryRow()
        self.WordGuess_Input.set_title(_("Mot"))

        # # Listen for keyboard event in the text box
        evk1 = Gtk.EventControllerKey()
        evk1.connect("key-pressed", input_event_handle, self)

        self.WordGuess_Input.connect("entry-activated", word_guess, self)
        self.WordGuess_Input.add_controller(evk1)
        self.Main_listbox.append(self.WordGuess_Input)

        # Current word guessed
        self.latest_word_guess_levelBar = Gtk.LevelBar()
        self.latest_word_guess_levelBar.set_mode(Gtk.LevelBarMode.CONTINUOUS)
        self.latest_word_guess_levelBar.set_value(0)
        self.latest_word_guess_levelBar.add_css_class("background-level-bar")
        self.latest_word_guess_levelBar.set_max_value(1000)
        self.latest_word_guess_levelBar.set_property("valign", Gtk.Align.CENTER)
        self.latest_word_guess_levelBar.set_property("width-request", 180)
        self.latest_word_guess_levelBar.set_min_value(0)

        self.emojiLabelCurrent = Gtk.Label(label="🟦")

        self.latest_word_guess = Adw.ActionRow()
        self.latest_word_guess.set_use_markup(False)
        self.latest_word_guess.set_title("-")
        # remove ability to focus, so it doesn't take focus when using up and down on the input above
        self.latest_word_guess.set_can_focus(False)
        self.latest_word_guess.set_subtitle("-")
        self.latest_word_guess.add_suffix(self.latest_word_guess_levelBar)
        self.latest_word_guess.add_prefix(self.emojiLabelCurrent)
        self.Main_listbox.append(self.latest_word_guess)

        self.Main_boxMain.set_content(self.Main_page)

        evk = Gtk.EventControllerKey.new()
        evk.connect("key-pressed", key_press, self)
        self.window.add_controller(evk)

        # Load all the UI from the different files
        load_main_header_ui(self)
        load_indice(self)
        load_how_to_play(self)
        load_shortcuts(self)
        load_settings(False, self)

        if len(self.available_language) > 0:
            load_model_and_wordlist(self)
        else:
            GLib.idle_add(self.share_button.set_sensitive, False)
            GLib.idle_add(self.hamburger.set_sensitive, False)
            GLib.idle_add(self.hamburgerLang.set_sensitive, False)
            self.Main_boxMain.set_content(self.noLangError)



def main():
    app = SemantiK()
    app.run(sys.argv)

if __name__ == "__main__":
    main()
