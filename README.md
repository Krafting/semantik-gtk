# SemantiK - Cémantix Clone GTK

## Screenshots

![Main App](./images/Screenshot-main-app-words.png)

## Roadmap

Here are the futur plan for this app, any help is appreciated!

- [ ] Create more language packs

Done:

- [x] Translation
- [x] Refactor and put stuff in multiple files for easier development
- [x] Additionnal "Language Pack" downloadable via Flathub. 

## Requirements

- < Python3.11
- GTK/Adw
- Gensim

## How to run

`python3.11 -m pip install -r requirements.txt`

`python3.11 SemantiK.py`

## How to build the flatpak

```shell
flatpak run org.flatpak.Builder --force-clean --sandbox --user --install --install-deps-from=flathub --ccache --mirror-screenshots-url=https://dl.flathub.org/media/ --repo=repo build-dir net.krafting.SemantiK.yml  && flatpak run net.krafting.SemantiK
```

Run linter to check for errors (Errors regarding screenshots or logos can be safely ignored)

```shell
flatpak run --command=flatpak-builder-lint org.flatpak.Builder manifest net.krafting.SemantiK.yml
flatpak run --command=flatpak-builder-lint org.flatpak.Builder repo repo
```

## How to generate language files

Generate basefile

```
xgettext -d base -o locales/base.pot -f locales/translation_files.text
```

Merge new message with existing translation:

```
msgmerge --update locales/en/LC_MESSAGES/base.po locales/base.pot
msgmerge --update locales/cs/LC_MESSAGES/base.po locales/base.pot
msgmerge --update locales/fr/LC_MESSAGES/base.po locales/base.pot
msgmerge --update locales/pt-br/LC_MESSAGES/base.po locales/base.pot
```

Generate mo files, **after** translating all the .po files

```
msgfmt -o locales/en/LC_MESSAGES/base.mo locales/en/LC_MESSAGES/base
msgfmt -o locales/cs/LC_MESSAGES/base.mo locales/cs/LC_MESSAGES/base
msgfmt -o locales/fr/LC_MESSAGES/base.mo locales/fr/LC_MESSAGES/base
msgfmt -o locales/pt-br/LC_MESSAGES/base.mo locales/pt-br/LC_MESSAGES/base
```