��    #      4  /   L           	          +     ;     B     U     a     r     �     �     �     �  Z  �  3   $     X     n     r     ~     �     �     �  E   �                  
   .     9     B  	   Y  0   c     �     �  .   �  	   �  �   �     �     �     �     �       	             -     >     E     Y     ^  <  v  #   �	     �	     �	     �	     �	  
   �	     
     
  @   
     ]
     b
     u
     �
     �
     �
     �
  3   �
     �
  
   �
  (        +                                 !   #                            
      "      	                                                                                 Chargement en cours Comment Jouer Comment Jouer ? Copier Demander un indice Données de Félicitations ! Idée originale par Importer Importer un Code de Mot. Indice Je ne connais pas ce mot. Le but du jeu est de trouver le mot secret en essayant de s'en approcher le plus possible contextuellement. Chaque mot se voit attribuer une température dont des valeurs intéressantes sont données en légende ci-dessous. Si votre mot se trouve dans les 1000 mots les plus proches, un indice de progression gradué de 1 à 1000 ‰ apparaîtra. Le nouveau mot a été appliqué. Amusez-vous bien! Merci de patienter... Mot Nouveau Mot OK Obtenir un indice Ouvrir la fenêtre de partage Partager Partagez votre mot actuel avec des amis. Ou importez le mot d'un ami. Quitter Raccourcis Clavier Raccourcis Globaux Résultats SemantiK Trouver le mot secret. Un Indice Un des 1000 mots les plus proches du mot secret. Votre Code de Mot Votre indice: Vous avez trouvé le mot en SCORE_WORDS coups. À Propos Project-Id-Version: 1.3.0
Report-Msgid-Bugs-To: 
PO-Revision-Date: 
Last-Translator: Krafting
Language-Team: 
Language: en
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 3.5
 Loading How to play How to play ? Copy Ask for a hint Data from Congratulations ! Original idea by Import Import a Word Code. Hint I don't know this word. The goal of the game is to find the secret word by trying to get as close as possible to it contextually. Each word is assigned a temperature, the interesting values ​​of which are given in the legend below. If your word is in the 1000 closest words, a progression index graduated from 1 to 1000 ‰ will appear. The new word was applied. Have fun! Please wait... Word New Word OK Get a hint Open sharing window Share Share your current word with friends. Or import a friend's word. Quit Keyboard Shortcuts Global Shortcuts Results SemantiK Find the secret word. A Hint One of the 1000 closest words from the secret word. Your Word Code Your hint: You found the word in SCORE_WORDS tries. About 