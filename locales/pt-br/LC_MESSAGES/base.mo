��    !      $  /   ,      �     �     �               !     -     >     R     [     t     {  Z  �  3   �     $     (     4     7     I     g  E   p     �     �     �  
   �     �     �  	     0        J     \  .   j  	   �  �  �  
   .     9  	   E     O     b  
   k  	   v  
   �      �     �     �  L  �  (    
     I
     V
     c
     f
  !   y
     �
  L   �
  	   �
     �
       
     
   *     5  
   Q  7   \     �     �  0   �     �                                                                 	                                               !                                           
    Comment Jouer Comment Jouer ? Copier Demander un indice Données de Félicitations ! Idée originale par Importer Importer un Code de Mot. Indice Je ne connais pas ce mot. Le but du jeu est de trouver le mot secret en essayant de s'en approcher le plus possible contextuellement. Chaque mot se voit attribuer une température dont des valeurs intéressantes sont données en légende ci-dessous. Si votre mot se trouve dans les 1000 mots les plus proches, un indice de progression gradué de 1 à 1000 ‰ apparaîtra. Le nouveau mot a été appliqué. Amusez-vous bien! Mot Nouveau Mot OK Obtenir un indice Ouvrir la fenêtre de partage Partager Partagez votre mot actuel avec des amis. Ou importez le mot d'un ami. Quitter Raccourcis Clavier Raccourcis Globaux Résultats SemantiK Trouver le mot secret. Un Indice Un des 1000 mots les plus proches du mot secret. Votre Code de Mot Votre indice: Vous avez trouvé le mot en SCORE_WORDS coups. À Propos Project-Id-Version: semantik-gtk 1.3.0
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2025-01-16 00:55-0300
Last-Translator: <johnppetersa@gmail.com>
Language-Team: Brazilian Portuguese <ldpbr-translation@lists.sourceforge.net>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 3.5
 Como jogar Como jogar? Copiadora Solicite uma pista Dados de Parabéns! Parabéns Importador Importar um código de palavras. Índice Eu não conheço esta palavra. O objetivo do jogo é encontrar a palavra segredo enquanto tenta abordá -lo o máximo possível contextualmente. Cada palavra recebe uma temperatura da qual valores atraentes são apresentados na legenda abaixo. Se sua palavra estiver nas 1000 palavras mais próximas, um índice de progressão graduado de 1 a 1000 ‰ aparecerá. A nova palavra foi aplicada. Divirta-se! Na direção Nova palavra OK Obtenha um índice Abra a janela de compartilhamento Compartilhar Compartilhe sua palavra atual com amigos. Ou importar a palavra de um amigo. Para sair Atalho de teclado Atalho global Resultados Semântica Encontre a palavra segredo. Um índice Uma das 1000 palavras mais próxima da palavra secreta. Seu código de palavra Seu índice: Você encontrou a palavra em score_words golpes. Sobre 