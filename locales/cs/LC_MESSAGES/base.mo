��    !      $  /   ,      �     �     �               !     -     >     R     [     t     {  Z  �  3   �     $     (     4     7     I     g  E   p     �     �     �  
   �     �     �  	     0        J     \  .   j  	   �  
  �  	   �  
   �  
   �     �  	   �     �     �            
        *  2  >  6   q	     �	     �	     �	     �	     �	     �	  =   �	     3
     <
     Q
  	   d
     n
     w
  
   �
  ,   �
     �
     �
  ,   �
  
                                                                    	                                               !                                           
    Comment Jouer Comment Jouer ? Copier Demander un indice Données de Félicitations ! Idée originale par Importer Importer un Code de Mot. Indice Je ne connais pas ce mot. Le but du jeu est de trouver le mot secret en essayant de s'en approcher le plus possible contextuellement. Chaque mot se voit attribuer une température dont des valeurs intéressantes sont données en légende ci-dessous. Si votre mot se trouve dans les 1000 mots les plus proches, un indice de progression gradué de 1 à 1000 ‰ apparaîtra. Le nouveau mot a été appliqué. Amusez-vous bien! Mot Nouveau Mot OK Obtenir un indice Ouvrir la fenêtre de partage Partager Partagez votre mot actuel avec des amis. Ou importez le mot d'un ami. Quitter Raccourcis Clavier Raccourcis Globaux Résultats SemantiK Trouver le mot secret. Un Indice Un des 1000 mots les plus proches du mot secret. Votre Code de Mot Votre indice: Vous avez trouvé le mot en SCORE_WORDS coups. À Propos Project-Id-Version: 1.3.0
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-11-16 15:21+0100
Last-Translator: Jaroslav Svoboda
Language-Team: 
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 3.5
 Jak hrát Jak hrát? Kopírovat Požádat o nápovědu Zdroj dat Gratulujeme! Námět Import Vložte kód slova. Nápověda Toto slovo neznám. Cílem hry je najít tajné slovo tím, že se mu pokusíte co nejvíce přiblížit kontextově. Každému slovu je přiřazena teplota, jejíž zajímavé hodnoty jsou uvedeny v legendě níže. Pokud je vaše slovo v nejbližších 1000 slovech, objeví se index pokroku odstupňovaný od 1 do 1000 ‰. Nové slovo bylo nastaveno. Přejeme skvělou zábavu! Slovo Nové slovo OK Požádat o nápovědu Otevřít okno sdílení Sdílet Sdílejte toto slovo s přáteli nebo získejte jejich slovo. Ukončit Klávesové zktratky Globální zkratky Výsledky SemantiK Uhodněte tajné  slovo. Nápověda Jedno z 1000 slov nejblíže tajnému slovu. Kód vašeho slova Vaše nápověda: Slovo bylo nalezeno po SCORE_WORDS pokusech. O aplikaci 