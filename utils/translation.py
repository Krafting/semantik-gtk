import gettext
import logging as log

def load_translation(self):
    """
    Function to load the translations text with gettext.
        - Try to load the whole language; (e.g. fr-FR) 
        - If it fail, try to load with only the first part (e.g. fr)
        - Or if it fails again, fallback to french
    """
    try:
        try:
            log.info(f"Using interface langage: {self.default_language}")
            lang_translations = gettext.translation("base", localedir="/app/locales/", languages=[self.default_language])
        except Exception as error:
            truncated_language = self.default_language.split("-")[0]
            log.info(f"Using interface langage: {truncated_language}")
            lang_translations = gettext.translation("base", localedir="/app/locales/", languages=[truncated_language])
    except Exception as error:
        log.error(f"Cannot load lang {self.default_language}: {error}")
        lang_translations = gettext.translation("base", localedir="/app/locales/", languages=["fr"])

    lang_translations.install()
    # define _ shortcut for translations
    return lang_translations.gettext
