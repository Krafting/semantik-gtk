import gi
import logging as log

gi.require_version('Gtk', '4.0')
from gi.repository import Gtk, Gdk

def load_custom_css():
    """
    Function to load custom CSS within the application.
    """
    # Custom CSS
    cssLevelBar = """
    levelbar > trough > block.filled {
        background-image: linear-gradient(
            90deg,
            rgb(246,211,45),
            rgb(255,120,0),
            rgb(224,27,36)
        );
    }
    .success-action-row-button {
        background: #76d08a;
    }
    .success-action-row-button:hover {
        background: #6bbd7d;
    }
    .success-action-row {
        background: #8ff0a4;
        color: #125B39;
    }"""

    display = Gdk.Display.get_default()

    css_provider = Gtk.CssProvider()
    css_provider.load_from_string(cssLevelBar)
    Gtk.StyleContext.add_provider_for_display(
        display,
        css_provider,
        Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION,
    )
