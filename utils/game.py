import logging as log
import re
import random
import gi
import json
import os
import threading
import base64

from gensim.models import KeyedVectors

gi.require_version('Gtk', '4.0')
gi.require_version('Adw', '1')
from gi.repository import Gtk, Adw, Gio, GLib

from utils.ui.toast import *
from utils.ui.sharing import *
from utils.utilities import *

def load_model_and_wordlist(self, is_from_settings=False):
    """
    Function used to call the function to fetch a reddit image
    """
    # Start the asynchronous request in a separate thread
    self.LoadingThread = threading.Thread(target=load_model_and_wordlist_async, args=(self, is_from_settings,))
    self.LoadingThread.start()
    return


def load_model_and_wordlist_async(self, is_from_settings = False):
    """
    Function to load the right model
    Used to load models on the fly when changing settings!
    """
    self.model_file_name = f"/app/share/extra/{self.config['set_langue_name']}/model.bin"
    log.info(f"Loading language model file: {self.model_file_name}")

    self.random_word_list = f"/app/share/extra/{self.config['set_langue_name']}/words.list"
    log.info(f"Loading language word file: {self.random_word_list}")

    # We show the loading page
    GLib.idle_add(self.share_button.set_sensitive, False)
    GLib.idle_add(self.hamburger.set_sensitive, False)
    GLib.idle_add(self.hamburgerLang.set_sensitive, False)
    self.Main_boxMain.set_content(self.Main_pageLoading)

    # Try to load the model and the wordlist we selected, if it fails, something has gone wrong, loading the default instead
    try:
        # Load the model file
        self.model = KeyedVectors.load_word2vec_format(str(self.model_file_name), binary=True, unicode_errors="ignore")
        log.info(f"Loaded model file: {self.model_file_name}")

        # Load the wordlist
        self.all_word_list = open(str(self.random_word_list)).read().splitlines()
        log.info(f"Loaded words file: {self.random_word_list}")

    except Exception as error:
        log.error(f"Error: failed to load lang model ({error})")

        # Page shown when there is a problem when loading langpack
        # /!\ : Move this codeblock with extreme precaution:
        #       Exception blocks are located in another place in the memory,
        #       so the 'self' object should be used wisely in these conditions

        errorField: Gtk.Entry = Gtk.Entry(editable=False, enable_undo=False, placeholder_text=_("Aucun message d'erreur disponible"), text=str(error))
        errorField.add_css_class('error')
        errorClampContainer: Adw.Clamp = Adw.Clamp(maximum_size=500, child=errorField)
        langpackLoadError: Adw.StatusPage = Adw.StatusPage(title=_('Impossible de charger le paquet de langues'), description=_('Une erreur est survenue lors du chargement du paquet de langues.\nVeuillez essayer de changer la langue dans le menu de langue.'), icon_name='mail-mark-junk-symbolic', child=errorClampContainer)
        GLib.idle_add(self.hamburgerLang.set_sensitive, True)
        GLib.idle_add(self.Main_boxMain.set_content, langpackLoadError)
        GLib.idle_add(load_sharing, self, True)
        return

    self.current_word_to_find = random.choice(self.all_word_list)
    self.current_word_to_find_code_sharing = base64.b64encode(self.current_word_to_find.encode("utf-8")).decode("ascii")

    # If we call the function to change the langue, or else we call it for the first launch so we initialize the app
    if is_from_settings:
        new_word(self=self)
    else:    
        load_sharing(self)
        load_last_word(self)
        self.most_similar_1 = find_pourmillage(self, 1)
        self.most_similar_10 = find_pourmillage(self, 10)
        self.most_similar_100 = find_pourmillage(self, 100)
        self.most_similar_1000 = find_pourmillage(self, 1000)
        update_similarity_page(self)

        add_to_save_last_word(self, word_to_guess=self.current_word_to_find)

    # We restore the page
    GLib.idle_add(self.share_button.set_sensitive, True)
    GLib.idle_add(self.hamburger.set_sensitive, True)
    GLib.idle_add(self.hamburgerLang.set_sensitive, True)
    self.Main_boxMain.set_content(self.Main_page)



def add_to_save_last_word(self, word_to_guess="", guessed_word="", reset=False) -> None:
    """
    Function to add a word to a save file. Used to load the
    """
    if word_to_guess != "":
        self.latest_game["word_to_guess"] = base64.b64encode(word_to_guess.encode("utf-8")).decode("ascii")
    elif reset is True:
        self.latest_game["guessed_word"] = []
    else:
        self.latest_game["guessed_word"] = self.all_guesses_order

    json.dump(self.latest_game, open(f"{self.config_folder}/latest_game.json", "w"))

def load_last_word(self) -> None:
    """
    Function to load the last word at startup and adding everything needed to get back to guessing!
    """
    if not os.path.exists(f"{self.config_folder}/latest_game.json"):
        return False

    try:
        # Load the latest game data and decode the word to guess
        self.latest_game = json.load(open(f"{self.config_folder}/latest_game.json"))

        if len(self.latest_game["guessed_word_unordered"]) <= 0:
            log.info("No word were guessed when the app was lastly used. Generating a new word...")
            return False
        self.current_word_to_find = str(base64.b64decode(self.latest_game["word_to_guess"].encode("ascii")).decode("utf-8"))

        # load most_similar values, or else they are not loaded
        self.most_similar_1 = find_pourmillage(self, 1)
        self.most_similar_10 = find_pourmillage(self, 10)
        self.most_similar_100 = find_pourmillage(self, 100)
        self.most_similar_1000 = find_pourmillage(self, 1000)

        # Update the Word Code
        self.ShareWord_actionrow.set_subtitle(self.latest_game["word_to_guess"])
        self.ShareWord_actionrowButton.connect("clicked", copy_stuff, self.latest_game["word_to_guess"], self)

        for word in self.latest_game["guessed_word"][::-1]:
            word_guess(self=self, word_passed=word["word"])
        return True
    except Exception as e:
        log.error(e)
        log.error("Cannot load latest game data")
        return False


def rearange_guessed_word(self, new_array=[], wordGuessedItem=""):
    """
    Function to rearrange all guessed word in the UI
    """
    for value in new_array:
        current_item_actionrow = self.all_guesses_variables[str(value["word"]) + "Main_ActionRowImported"]
        log.debug(f"reorganizing: {value['word']} {value['score']}")
        self.Main_listboxResults.remove(current_item_actionrow)
        self.Main_listboxResults.append(current_item_actionrow)
    return new_array

def word_guess(event = "", self = False, word_passed = ""):
    """
    Function used when sending a word via the main Input
    """
    # reset the history when sending a word
    self.current_history_pos = 0
    # We remove all special chars from the current string
    # We can add a word to the word list if word_passed is not empty
    if word_passed == "":
        word_guess_user = re.sub(r"[^a-zA-ZÀ-ÿ-]+", "", self.WordGuess_Input.get_text()).lower()
        self.latest_game["guessed_word_unordered"].append(str(word_guess_user))
    else:
        word_guess_user = re.sub(r"[^a-zA-ZÀ-ÿ-]+", "", word_passed).lower()

    score = get_word_score(self, word_guess_user)

    pourmillage = get_pourmille_from_score(self, score)

    # Don't put negative number in the level bar
    if pourmillage < 1:
        pourmillageLevelBar = 0
    else:
        pourmillageLevelBar = pourmillage

    # If the word doesn't exist..
    if word_guess_user == "":
        return
    if score is False or not is_valid_word(word_guess_user):
        self.WordGuess_Input.set_text("")
        self.latest_word_guess_levelBar.set_value(0)
        self.latest_word_guess.set_title(str(word_guess_user))
        self.emojiLabelCurrent.set_label("🟥")
        self.latest_word_guess.set_subtitle(_("Je ne connais pas ce mot."))
        return

    log.debug(str(word_guess_user))

    if not hasattr(self, "Main_listboxResults"):
        self.LabelResults = Gtk.Label(label=_("Résultats"))
        self.LabelResults.add_css_class("title-3")
        self.LabelResults.set_margin_bottom(15)
        self.LabelResults.set_margin_top(15)
        self.LabelResults.set_property("valign", Gtk.Align.CENTER)
        self.Main_box2.append(self.LabelResults)

        # Boite contenant les résultats
        self.Main_listboxResults = Gtk.ListBox()
        self.Main_listboxResults.set_selection_mode(Gtk.SelectionMode.NONE)
        self.Main_listboxResults.add_css_class("boxed-list")
        self.Main_box2.append(self.Main_listboxResults)

    self.emojiLabel = Gtk.Label(label=str(get_score_emoji(self, score)))

    # Update the current word box
    self.latest_word_guess_levelBar.set_value(0)
    self.latest_word_guess_levelBar.set_value(pourmillageLevelBar)
    self.latest_word_guess.set_title(str(word_guess_user))
    self.emojiLabelCurrent.set_label(str(get_score_emoji(self, score)))
    self.latest_word_guess.set_subtitle(f"{score} °C - {pourmillage} ‰")

    # On reset l'animation, utile quand on écrit des mot rapidement
    if self.animation_timed != "":
        self.animation_timed.reset()

    target_timed = Adw.PropertyAnimationTarget.new(self.latest_word_guess_levelBar, "value")
    self.animation_timed = Adw.TimedAnimation.new(self.latest_word_guess_levelBar, 0, pourmillageLevelBar, 1800, target_timed)
    self.animation_timed.connect("done", is_word_found, pourmillage, self)
    self.animation_timed.play()

    if str(word_guess_user) + "levelBarResult" in self.all_guesses_variables:
        self.WordGuess_Input.set_text("")
        return

    self.all_guesses_variables[word_guess_user + "levelBarResult"] = Gtk.LevelBar()
    self.all_guesses_variables[word_guess_user + "levelBarResult"].set_mode(Gtk.LevelBarMode.CONTINUOUS)
    self.all_guesses_variables[word_guess_user + "levelBarResult"].set_value(pourmillageLevelBar)
    self.all_guesses_variables[word_guess_user + "levelBarResult"].set_max_value(1000)
    self.all_guesses_variables[word_guess_user + "levelBarResult"].set_property("valign", Gtk.Align.CENTER)
    self.all_guesses_variables[word_guess_user + "levelBarResult"].set_property("width-request", 180)
    self.all_guesses_variables[word_guess_user + "levelBarResult"].set_min_value(0)

    self.all_guesses_order.append({"word": str(word_guess_user), "score": float(score)})

    # Override current array with a sorted one.
    self.all_guesses_order = sort_guess_array(self.all_guesses_order)

    self.all_guesses_variables[word_guess_user + "Main_ActionRowImported"] = Adw.ActionRow()
    self.all_guesses_variables[word_guess_user + "Main_ActionRowImported"].set_use_markup(False)
    self.all_guesses_variables[word_guess_user + "Main_ActionRowImported"].set_title(str(word_guess_user))
    self.all_guesses_variables[word_guess_user + "Main_ActionRowImported"].add_suffix(
        self.all_guesses_variables[word_guess_user + "levelBarResult"]
    )
    self.all_guesses_variables[word_guess_user + "Main_ActionRowImported"].add_prefix(self.emojiLabel)
    self.all_guesses_variables[word_guess_user + "Main_ActionRowImported"].set_subtitle(f"{score} °C - {pourmillage} ‰")
    self.Main_listboxResults.append(self.all_guesses_variables[word_guess_user + "Main_ActionRowImported"])

    rearange_guessed_word(self, self.all_guesses_order, self.all_guesses_variables[word_guess_user + "Main_ActionRowImported"])

    # Animation for the guessed word (right panel)
    target_timed = Adw.PropertyAnimationTarget.new(self.all_guesses_variables[word_guess_user + "levelBarResult"], "value")
    animation_timed_word = Adw.TimedAnimation.new(
        self.all_guesses_variables[word_guess_user + "levelBarResult"], 0, pourmillageLevelBar, 2000, target_timed
    )
    animation_timed_word.play()

    # If we write a new word in the text box, we save it to the save file
    if word_passed == "":
        add_to_save_last_word(self, guessed_word=word_guess_user)

    # Reset the input after entering a word
    self.WordGuess_Input.set_text("")

def is_word_found(event, pourmillage, self):
    """
    Function called after each animation, to show the congrutation box after the animation is played
    """
    if pourmillage >= 1000:
        nombre_de_coup = len(self.all_guesses_order)
        # Show the box
        self.Main_listboxFoundWord.set_visible(True)
        self.Main_felicitationsActionRow.set_subtitle(
            _("Vous avez trouvé le mot en SCORE_WORDS coups.").replace("SCORE_WORDS", str(nombre_de_coup))
        )

def find_pourmillage(self, similarity=0):
    """
    Function to find the x most similar word and its score.
    """
    score = self.model.most_similar(self.current_word_to_find, topn=similarity)
    return score[-1][1]

def update_similarity_page(self):
    """
    Function to update the how to play page with the current similarity values
    """
    self.HowToPlayMeaning_actionrow1.set_subtitle(f"{self.most_similar_1 * 100:.2f} °C")
    self.HowToPlayMeaning_actionrow10.set_subtitle(f"{self.most_similar_10 * 100:.2f} °C")
    self.HowToPlayMeaning_actionrow100.set_subtitle(f"{self.most_similar_100 * 100:.2f} °C")
    self.HowToPlayMeaning_actionrow1000.set_subtitle(f"{self.most_similar_1000 * 100:.2f} °C")

    self.emojiLabelRules0.set_label(str(get_score_emoji(self, 100)))
    self.emojiLabelRules1.set_label(str(get_score_emoji(self, self.most_similar_1 * 100)))
    self.emojiLabelRules10.set_label(str(get_score_emoji(self, self.most_similar_10 * 100)))
    self.emojiLabelRules100.set_label(str(get_score_emoji(self, self.most_similar_100 * 100)))
    self.emojiLabelRules1000.set_label(str(get_score_emoji(self, self.most_similar_1000 * 100)))

def reset_all_with_new_word(self):
    """
    Function to reset forms and put everything back to default (used when asking a new word)
    """
    # Reset the current found word actionrow
    self.latest_word_guess.set_title("-")
    self.latest_word_guess.set_subtitle("-")
    self.emojiLabelCurrent.set_label(str(get_score_emoji(self, -1000)))
    self.latest_word_guess_levelBar.set_value(0)

    # Remove all guessed words
    self.all_guesses_variables = {}
    self.all_guesses_order = []
    self.animation_timed = ""
    self.current_history_pos = 0
    self.latest_game = {}
    self.latest_game["guessed_word"] = []
    self.latest_game["guessed_word_unordered"] = []
    add_to_save_last_word(self, reset=True)

    # Remove all guessed words actionrows
    if hasattr(self, "LabelResults"):
        self.Main_listboxResults.remove_all()
        self.Main_box2.remove(self.Main_listboxResults)
        self.Main_box2.remove(self.LabelResults)
        del self.Main_listboxResults
        del self.LabelResults

    # Hide the congratz box
    self.Main_listboxFoundWord.set_visible(False)

    # Fix the Copy button
    self.ShareWord_actionrowButton.connect("clicked", copy_stuff, self.current_word_to_find_code_sharing, self)

def new_word(event = "", other_event = "", self = False, word = "", code = "", list_words = []):
    """
    Function to apply a new word, from the sharing page
    """
    # If we give the function a list
    if len(list_words) > 0:
        list_to_choose_word = list_words
    else:
        list_to_choose_word = self.all_word_list

    if code != "":
        self.current_word_to_find = str(base64.b64decode(code.encode("ascii")).decode("utf-8"))
    elif word != "":
        self.current_word_to_find = str(word)
    else:
        self.current_word_to_find = str(random.choice(list_to_choose_word))

    self.most_similar_1 = find_pourmillage(self, 1)
    self.most_similar_10 = find_pourmillage(self, 10)
    self.most_similar_100 = find_pourmillage(self, 100)
    self.most_similar_1000 = find_pourmillage(self, 1000)

    update_similarity_page(self)

    # Update the share Word Code
    self.current_word_to_find_code_sharing = base64.b64encode(self.current_word_to_find.encode("utf-8")).decode("ascii")
    self.ShareWord_actionrow.set_subtitle(self.current_word_to_find_code_sharing)

    reset_all_with_new_word(self)

    add_to_save_last_word(self, word_to_guess=self.current_word_to_find)

    # If we ask for a new word
    if event != "":
        add_toast(_("Le nouveau mot a été appliqué. Amusez-vous bien!"), self=self)




def get_word_score(self, word_guessed, current_word_to_find=""):
    """
    Function to compare the guessed word to the real word to find
    """
    try:
        similarity = self.model.similarity(self.current_word_to_find, word_guessed)
        return float("{:.2f}".format(similarity * 100))
    except:
        return False
