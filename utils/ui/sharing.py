import gi 
import logging as log

gi.require_version('Gtk', '4.0')
gi.require_version('Pango', '1.0')
gi.require_version('Adw', '1')
from gi.repository import Gtk, Adw, Gio, Gdk, Graphene, GLib, GObject, Pango

from utils.utilities import *
from utils.ui.toast import *
import utils.game as game

def show_sharing(action = None, param = None, self = False) -> None:
    """
    Function to show the sharing window
    """
    self.ShareWord_application_dialog.present(self.get_active_window())

def load_sharing(self, placeholder = False):
    """
    Function to load the UI related to the sharing menu
    """
    self.ShareWord_application_dialog = Adw.Dialog()
    self.ShareWord_application_dialog.set_title(_("Partager"))
    self.ShareWord_application_dialog.set_content_width(600)
    self.ShareWord_boxMain = Adw.ToolbarView()

    # We can still load the UI when the model failed to load.
    if placeholder == True:
        self.current_word_to_find_code_sharing = ''

    # New stream Header bar
    self.ShareWord_header = Adw.HeaderBar()
    self.ShareWord_boxMain.add_top_bar(self.ShareWord_header)

    self.record_button = Gtk.Button(label=_("OK"))
    self.ShareWord_header.pack_end(self.record_button)
    self.record_button.set_icon_name("plus-large-symbolic")
    self.record_button.set_label(_("OK"))
    self.record_button.add_css_class("suggested-action")
    self.record_button.connect("clicked", ok_sharing, self)

    # Set window child
    self.ShareWord_application_dialog.set_child(self.ShareWord_boxMain)

    self.ShareWord_page = Adw.StatusPage()
    self.ShareWord_page.set_title(_("Partager"))
    self.ShareWord_page.set_description(_("Partagez votre mot actuel avec des amis. Ou importez le mot d'un ami."))

    self.ShareWord_clamp = Adw.Clamp()
    self.ShareWord_page.set_child(self.ShareWord_clamp)

    self.ShareWord_box2 = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
    self.ShareWord_clamp.set_child(self.ShareWord_box2)

    self.ShareWord_listbox = Gtk.ListBox()
    self.ShareWord_listbox.set_selection_mode(Gtk.SelectionMode.NONE)
    self.ShareWord_box2.append(self.ShareWord_listbox)
    self.ShareWord_listbox.add_css_class("boxed-list")

    self.ShareWord_actionrowButton = Gtk.Button(label=_("Copier"))
    self.ShareWord_actionrowButton.set_valign(Gtk.Align.CENTER)
    self.ShareWord_actionrowButton.connect("clicked", copy_stuff, self.current_word_to_find_code_sharing, self)

    self.ShareWord_actionrow = Adw.ActionRow()
    self.ShareWord_actionrow.set_title(_("Votre Code de Mot"))
    self.ShareWord_actionrow.add_suffix(self.ShareWord_actionrowButton)
    self.ShareWord_actionrow.set_subtitle(self.current_word_to_find_code_sharing)
    self.ShareWord_listbox.append(self.ShareWord_actionrow)

    self.LabelSharing = Gtk.Label(label=_("Importer"))
    self.LabelSharing.add_css_class("title-3")
    self.LabelSharing.set_margin_bottom(15)
    self.LabelSharing.set_margin_top(15)
    self.ShareWord_box2.append(self.LabelSharing)

    self.ImportWord_listbox = Gtk.ListBox()
    self.ImportWord_listbox.set_selection_mode(Gtk.SelectionMode.NONE)
    self.ShareWord_box2.append(self.ImportWord_listbox)
    self.ImportWord_listbox.add_css_class("boxed-list")

    self.ImportWord_entryrow = Adw.EntryRow()
    self.ImportWord_entryrow.set_title(_("Importer un Code de Mot."))
    self.ImportWord_listbox.append(self.ImportWord_entryrow)

    # Add the page to the window
    self.ShareWord_boxMain.set_content(self.ShareWord_page)

def ok_sharing(event, self):
    """
    Function when the sharing window is updated, also change the word with the code
    """
    import_word_text = self.ImportWord_entryrow.get_text()
    if import_word_text != "":
        try:
            # Ask a new word (and reset everything)
            game.new_word(self=self, code=import_word_text)

            # Show a toast
            add_toast(text=_("Le nouveau mot a été appliqué. Amusez-vous bien!"), self=self)

            # Reset the Sharing word box
            self.ImportWord_entryrow.set_text("")

        except Exception as e:
            log.error(e)
            log.warning("Cannot decode Word Code.")
    self.ShareWord_application_dialog.close()