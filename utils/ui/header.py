import gi

gi.require_version('Gtk', '4.0')
gi.require_version('Adw', '1')
from gi.repository import Gtk, Adw, Gio

# Utils
from utils.game import *
from utils.utilities import *
from utils.ui.shortcuts import *
from utils.ui.settings import *
from utils.ui.how_to_play import *
from utils.ui.indice import *
from utils.ui.sharing import *
from utils.ui.dialogs import *
from utils.ui.about import *

def load_main_header_ui(self) -> None: 
    """
    Function to load the main header UI
    """
    self.header = Adw.HeaderBar()
    self.Main_boxMain.add_top_bar(self.header)

    self.share_button = Gtk.Button(label=_("Partager"))
    self.header.pack_start(self.share_button)
    self.share_button.set_icon_name("emblem-shared-symbolic")
    self.share_button.connect("clicked", show_sharing, None, self)

    # Create a popover
    self.menu = Gio.Menu.new()
    self.popover = Gtk.PopoverMenu()  # Create a new popover menu
    self.popover.set_menu_model(self.menu)

    # Create a menu button
    self.hamburger = Gtk.MenuButton()
    self.hamburger.set_popover(self.popover)
    self.hamburger.set_icon_name("open-menu-symbolic")  # Give it a nice icon

    # Add menu button to the header bar (at the end)
    self.header.pack_end(self.hamburger)

    # Request a new word
    action_new_word = Gio.SimpleAction.new("new_word_generate", None)
    action_new_word.connect("activate", new_word, self)
    self.window.add_action(action_new_word)

    action_how_to_play = Gio.SimpleAction.new("show_how_to_play", None)
    action_how_to_play.connect("activate", show_how_to_play, self)
    self.window.add_action(action_how_to_play)

    # Indices
    action_show_indice = Gio.SimpleAction.new("show_indice", None)
    action_show_indice.connect("activate", show_indice, self)
    self.window.add_action(action_show_indice)

    # Add shortcuts dialog
    action_shortcuts = Gio.SimpleAction.new("shortcuts", None)
    action_shortcuts.connect("activate", show_shortcuts, self)
    self.window.add_action(action_shortcuts)

    # Add an about dialog
    action_about = Gio.SimpleAction.new("about", None)
    action_about.connect("activate", show_about, self)
    self.window.add_action(action_about)

    # We add everything and their action to the menu.
    self.menu.append(_("Obtenir un indice"), "win.show_indice")
    self.menu.append(_("Nouveau Mot"), "win.new_word_generate")

    self.menu_section_1 = Gio.Menu.new()
    self.menu_section_1.append(_("Comment Jouer ?"), "win.show_how_to_play")
    self.menu.insert_section(3, None, self.menu_section_1)

    self.menu_section_2 = Gio.Menu.new()
    self.menu_section_2.append(_("Raccourcis Clavier"), "win.shortcuts")
    self.menu_section_2.append(_("À Propos"), "win.about")
    self.menu.insert_section(3, None, self.menu_section_2)

    ## Language selector

    # Create a popover
    self.menuLang = Gio.Menu.new()
    self.popoverLang = Gtk.PopoverMenu()  # Create a new popover menu
    self.popoverLang.set_menu_model(self.menuLang)

    # Create a menu button for languages
    self.hamburgerLang = Gtk.MenuButton()
    self.hamburgerLang.set_popover(self.popoverLang)
    self.hamburgerLang.set_icon_name("language-symbolic")  # Give it a nice icon

    # Add menu button to the header bar (at the end)
    self.header.pack_end(self.hamburgerLang)


    # Create the menu
    self.menu_section_0_lang = Gio.Menu.new()
    
    # Populate the Language Menu

    # Create the action
    self.action_show_changeLang = Gio.SimpleAction.new_stateful("change_language", GLib.VariantType('s'), GLib.Variant('s', 'Français'))
    self.action_show_changeLang.connect("activate", change_language, self)
    self.window.add_action(self.action_show_changeLang)

    for lang in self.available_language:
        self.menu_section_0_lang.append(str(lang), "win.change_language::" + str(lang))

    self.menuLang.insert_section(3, None, self.menu_section_0_lang)

    self.menu_section_1_lang = Gio.Menu.new()
    # Open the App Store
    action_show_openStore = Gio.SimpleAction.new("openStore", None)
    action_show_openStore.connect("activate", open_store, self)
    self.window.add_action(action_show_openStore)
    self.menu_section_1_lang.append(_("Obtenir Plus"), "win.openStore")
    self.menuLang.insert_section(3, None, self.menu_section_1_lang)

