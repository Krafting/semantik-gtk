import gi 
import random
import logging as log

gi.require_version('Gtk', '4.0')
gi.require_version('Pango', '1.0')
gi.require_version('Adw', '1')
from gi.repository import Gtk, Adw, Gio, Gdk, Graphene, GLib, GObject, Pango

from utils.utilities import *

def show_indice(action = None, param = None, self = False) -> None:
    """
    Function to show the window of the indice page and also loading the indice
    """
    incide_list = self.model.most_similar(self.current_word_to_find, topn=1000)
    indice_choosen = random.choice(incide_list)

    self.IndicePage_actionrow.set_title(str(indice_choosen[0]))
    self.IndicePage_actionrow.set_subtitle(str("{:.2f}".format(indice_choosen[1] * 100)))
    self.IndicePage_actionrowButton.connect("clicked", copy_stuff, str(indice_choosen[0]), self)

    self.IndicePage_application_dialog.present(self.get_active_window())

def load_indice(self):
    """
    Function to load the UI related to the indice page
    """
    self.IndicePage_application_dialog = Adw.Dialog()
    self.IndicePage_application_dialog.set_title(_("Indice"))
    self.IndicePage_application_dialog.set_content_width(620)
    self.IndicePage_boxMain = Adw.ToolbarView()

    # New stream Header bar
    self.IndicePage_header = Adw.HeaderBar()
    self.IndicePage_boxMain.add_top_bar(self.IndicePage_header)

    # Set window child
    self.IndicePage_application_dialog.set_child(self.IndicePage_boxMain)

    self.IndicePage_page = Adw.StatusPage()
    self.IndicePage_page.set_title(_("Un Indice"))
    self.IndicePage_page.set_description(_("Un des 1000 mots les plus proches du mot secret."))

    self.IndicePage_clamp = Adw.Clamp()
    self.IndicePage_page.set_child(self.IndicePage_clamp)
    self.IndicePage_box2 = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
    self.IndicePage_clamp.set_child(self.IndicePage_box2)

    self.IndicePage_listbox = Gtk.ListBox()
    self.IndicePage_listbox.set_selection_mode(Gtk.SelectionMode.NONE)
    self.IndicePage_box2.append(self.IndicePage_listbox)
    self.IndicePage_listbox.add_css_class("boxed-list")

    self.IndicePage_actionrowButton = Gtk.Button(label=_("Copier"))
    self.IndicePage_actionrowButton.set_valign(Gtk.Align.CENTER)

    self.IndiceEmoji = Gtk.Label(label="🌟")

    self.IndicePage_actionrow = Adw.ActionRow()
    self.IndicePage_actionrow.set_title(_("Votre indice:"))
    self.IndicePage_actionrow.add_prefix(self.IndiceEmoji)
    self.IndicePage_actionrow.add_suffix(self.IndicePage_actionrowButton)
    self.IndicePage_listbox.append(self.IndicePage_actionrow)

    # Add the page to the window
    self.IndicePage_boxMain.set_content(self.IndicePage_page)

