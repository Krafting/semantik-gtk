import gi 
import logging as log

gi.require_version('Gtk', '4.0')
gi.require_version('Pango', '1.0')
gi.require_version('Adw', '1')
from gi.repository import Gtk, Adw, Gio, Gdk, Graphene, GLib, GObject, Pango

def show_how_to_play(action = None, param = None, self=False) -> None:
    """
    Function to show the help and how to play
    """
    self.HowToPlayMeaning_application_dialog.present(self.get_active_window())

def load_how_to_play(self):
    """
    Function to load the UI related to the How To Play menu
    """
    self.HowToPlayMeaning_application_dialog = Adw.Dialog()
    self.HowToPlayMeaning_application_dialog.set_title(_("Comment Jouer"))
    self.HowToPlayMeaning_application_dialog.set_content_width(650)

    self.HowToPlayMeaning_boxMain = Adw.ToolbarView()

    # New stream Header bar
    self.HowToPlayMeaning_header = Adw.HeaderBar()
    self.HowToPlayMeaning_boxMain.add_top_bar(self.HowToPlayMeaning_header)

    # Set window child
    self.HowToPlayMeaning_application_dialog.set_child(self.HowToPlayMeaning_boxMain)

    self.HowToPlayMeaning_page = Adw.StatusPage()
    self.HowToPlayMeaning_page.set_title(_("Comment Jouer"))
    self.HowToPlayMeaning_page.set_description(
        _(
            "Le but du jeu est de trouver le mot secret en essayant de s'en approcher le plus possible contextuellement. Chaque mot se voit attribuer une température dont des valeurs intéressantes sont données en légende ci-dessous. Si votre mot se trouve dans les 1000 mots les plus proches, un indice de progression gradué de 1 à 1000 ‰ apparaîtra."
        )
    )

    self.HowToPlayMeaning_clamp = Adw.Clamp()
    self.HowToPlayMeaning_page.set_child(self.HowToPlayMeaning_clamp)

    self.HowToPlayMeaning_box2 = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
    self.HowToPlayMeaning_clamp.set_child(self.HowToPlayMeaning_box2)

    self.HowToPlayMeaning_listbox = Gtk.ListBox()
    self.HowToPlayMeaning_listbox.set_selection_mode(Gtk.SelectionMode.NONE)
    self.HowToPlayMeaning_box2.append(self.HowToPlayMeaning_listbox)
    self.HowToPlayMeaning_listbox.add_css_class("boxed-list")

    self.HowToPlayMeaning_actionrow0 = Adw.ActionRow()
    self.HowToPlayMeaning_actionrow0.set_title("1000 ‰")
    self.HowToPlayMeaning_actionrow0.set_subtitle("100 °C")

    self.HowToPlayMeaning_actionrow1 = Adw.ActionRow()
    self.HowToPlayMeaning_actionrow1.set_title("999 ‰")
    self.HowToPlayMeaning_actionrow1.set_subtitle("")

    self.HowToPlayMeaning_actionrow10 = Adw.ActionRow()
    self.HowToPlayMeaning_actionrow10.set_title("990 ‰")
    self.HowToPlayMeaning_actionrow10.set_subtitle("")

    self.HowToPlayMeaning_actionrow100 = Adw.ActionRow()
    self.HowToPlayMeaning_actionrow100.set_title("900 ‰")
    self.HowToPlayMeaning_actionrow100.set_subtitle("")

    self.HowToPlayMeaning_actionrow1000 = Adw.ActionRow()
    self.HowToPlayMeaning_actionrow1000.set_title("1 ‰")
    self.HowToPlayMeaning_actionrow1000.set_subtitle("")

    self.emojiLabelRules0 = Gtk.Label(label="")
    self.emojiLabelRules1 = Gtk.Label(label="")
    self.emojiLabelRules10 = Gtk.Label(label="")
    self.emojiLabelRules100 = Gtk.Label(label="")
    self.emojiLabelRules1000 = Gtk.Label(label="")
    self.HowToPlayMeaning_actionrow1000.add_prefix(self.emojiLabelRules1000)
    self.HowToPlayMeaning_actionrow100.add_prefix(self.emojiLabelRules100)
    self.HowToPlayMeaning_actionrow10.add_prefix(self.emojiLabelRules10)
    self.HowToPlayMeaning_actionrow1.add_prefix(self.emojiLabelRules1)
    self.HowToPlayMeaning_actionrow0.add_prefix(self.emojiLabelRules0)

    self.HowToPlayMeaning_listbox.append(self.HowToPlayMeaning_actionrow0)
    self.HowToPlayMeaning_listbox.append(self.HowToPlayMeaning_actionrow1)
    self.HowToPlayMeaning_listbox.append(self.HowToPlayMeaning_actionrow10)
    self.HowToPlayMeaning_listbox.append(self.HowToPlayMeaning_actionrow100)
    self.HowToPlayMeaning_listbox.append(self.HowToPlayMeaning_actionrow1000)

    # Add the page to the window
    self.HowToPlayMeaning_boxMain.set_content(self.HowToPlayMeaning_page)

