import gi 
import logging as log

gi.require_version('Gtk', '4.0')
gi.require_version('Pango', '1.0')
gi.require_version('Adw', '1')
from gi.repository import Gtk, Adw, Gio, Gdk, Graphene, GLib, GObject, Pango

def show_about(action, param, self=False) -> None:
    """
    Function to create and show the AboutWindow
    """
    self.about = Adw.AboutDialog()
    self.about.set_application_icon("net.krafting.SemantiK")
    self.about.set_application_name("SemantiK")
    self.about.set_developers(["Krafting"])
    self.about.set_license_type(Gtk.License.GPL_3_0)
    self.about.add_credit_section(_("Idée originale par"), ["enigmatix"])
    self.about.add_credit_section(_("Données de"), ["Jean-Philippe Fauconnier", "Google"])
    self.about.add_credit_section(_("Contributeurs"), ["DodoLeDev"])
    self.about.set_version("1.5.0")
    self.about.set_website("https://gitlab.com/Krafting/semantik-gtk/")
    self.about.set_issue_url("https://gitlab.com/Krafting/semantik-gtk/-/issues")
    self.about.present(self.get_active_window())
