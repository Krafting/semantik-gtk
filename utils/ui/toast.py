import gi 
import logging as log

gi.require_version('Adw', '1')
from gi.repository import Adw

def add_toast(text, timeout = 3, self=False) -> None:
    """
    Function to create and display a toast on the main Window
    """
    # Dismiss previous Toast if there are any
    try:
        self.Main_ToastDisplay.dismiss()
    except:
        log.debug('No toast, nothing to do.')

    self.Main_ToastDisplay = Adw.Toast()
    self.Main_ToastDisplay.set_title(text)
    self.Main_ToastDisplay.set_timeout(timeout)
    self.Main_Toast.add_toast(self.Main_ToastDisplay)