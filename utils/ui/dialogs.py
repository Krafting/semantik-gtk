import gi 
import logging as log

gi.require_version('Gtk', '4.0')
gi.require_version('Adw', '1')
from gi.repository import Gtk, Adw, GLib

import utils.ui.settings as sets

def create_dialog(title, message, typed = "normal", self=False):
    """
    Function to easily create Dialog on demand.
    Typed can be use to replace buttons and actions of said button
    """
    # By using GLib.idle_add, we're ensuring that the GUI operations (in this case, creating the dialog) 
    # happen in the main thread, thus avoiding potential threading issues with GTK.
    def create_dialog_callback():
        # Remove previous dialogs
        self.Main_dialog = Adw.AlertDialog()
        self.Main_dialog.set_heading(str(title))
        self.Main_dialog.set_body(str(message))
        self.Main_dialog.set_body_use_markup(False)
        self.Main_dialog.set_close_response("close-dialog")
        
        if typed == "settings":
            self.Main_dialog.add_response('close-dialog', 'Close')
            self.Main_dialog.add_response('open-settings', 'Open Settings')
            self.Main_dialog.set_response_appearance('open-settings', Adw.ResponseAppearance.SUGGESTED)
        else:
            self.Main_dialog.add_response('close-dialog', 'Okay')

        self.Main_dialog.choose(self.get_active_window(), callback=on_response_selected, user_data=self)

    # Schedule the creation of the dialog in the main event loop
    GLib.idle_add(create_dialog_callback)


def on_response_selected(response, task, self=False):
    """
    Function to handle response of Dialogs
    """
    response = response.choose_finish(task)
    if response == "open-settings":
        sets.show_settings(None, None, self)
