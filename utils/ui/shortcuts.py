import gi 
import logging as log

gi.require_version('Gtk', '4.0')
gi.require_version('Pango', '1.0')
gi.require_version('Adw', '1')
from gi.repository import Gtk, Adw, Gio, Gdk, Graphene, GLib, GObject, Pango


def show_shortcuts(action = None, param = None, self=False) -> None:
    """
    Function to present (show) the Shortcuts window
    """
    self.shortcutwindow.present(self.get_active_window())


def load_shortcuts(self):

    #######################
    # SHORTCUTS DIALOG
    #######################
    self.shortcutwindow = Adw.Dialog()
    self.shortcutwindow.set_title("Shortcuts")

    # When using Dialog, we need to create a toolbar with the header, to show the close button...
    self.shortcutToolbarView = Adw.ToolbarView()
    self.shortcutHeader = Adw.HeaderBar()
    self.shortcutToolbarView.add_top_bar(self.shortcutHeader)

    self.shortcutwindowSection = Gtk.ShortcutsSection()

    self.shortcutwindowGroup = Gtk.ShortcutsGroup()
    self.shortcutwindowGroup.set_property("title", _("Raccourcis Globaux"))

    # Shortcut CTRL N - Open the sharing window
    self.shortcutShareWord = Gtk.ShortcutsShortcut()
    self.shortcutShareWord.set_property("accelerator", "<ctl>n")
    self.shortcutShareWord.set_property("title", _("Ouvrir la fenêtre de partage"))
    self.shortcutwindowGroup.add_shortcut(self.shortcutShareWord)

    # Shortcut CTRL I - Ask for a indice
    self.shortcutShareWord = Gtk.ShortcutsShortcut()
    self.shortcutShareWord.set_property("accelerator", "<ctl>i")
    self.shortcutShareWord.set_property("title", _("Demander un indice"))
    self.shortcutwindowGroup.add_shortcut(self.shortcutShareWord)

    # Shortcut CTRL Q - Quit
    self.shortcutQuit = Gtk.ShortcutsShortcut()
    self.shortcutQuit.set_property("accelerator", "<ctl>q")
    self.shortcutQuit.set_property("title", _("Quitter"))
    self.shortcutwindowGroup.add_shortcut(self.shortcutQuit)

    # Shortcut CTRL ? - Show shortcuts
    self.shortcutKBShortcuts = Gtk.ShortcutsShortcut()
    self.shortcutKBShortcuts.set_property("accelerator", "<ctl>question")
    self.shortcutKBShortcuts.set_property("title", _("Raccourcis Clavier"))
    self.shortcutwindowGroup.add_shortcut(self.shortcutKBShortcuts)

    # Add Groups to section
    self.shortcutwindowSection.add_group(self.shortcutwindowGroup)
    # Add section to Window
    self.shortcutToolbarView.set_content(self.shortcutwindowSection)
    self.shortcutwindow.set_child(self.shortcutToolbarView)
