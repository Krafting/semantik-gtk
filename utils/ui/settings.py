import gi 
import logging as log
import os
import json

gi.require_version('Gtk', '4.0')
gi.require_version('Pango', '1.0')
gi.require_version('Adw', '1')
from gi.repository import Gtk, Adw, Gio, Gdk, Graphene, GLib, GObject, Pango

from utils.ui.dialogs import *
import utils.game as game

def save_settings(action, param, self = False) -> None:
    """
    Function to save settings data to the self.config variable
    Then, write a settings.json file to the config folder of the app
    """
    changeLangState = str(self.action_show_changeLang.get_state()).replace("'", "")
    if self.config["set_langue_name"] != changeLangState:
        self.config["set_langue_name"] = str(changeLangState)
        if not self.is_loading_setting_ui:
            game.load_model_and_wordlist(self, is_from_settings=True)
    else:
        self.config["set_langue_name"] = str(changeLangState)

    json.dump(self.config, open(f"{self.config_folder}/settings.json", "w"))


def load_settings(just_load_value = False, self = False) -> None:
    """
    Function to load the settings data at startup of the application
    This is a bit hacky, when you load a value, it send the signal to save settings and overwriting 
    everything else, so we store the config in variable, change the values we want and then load it again
    """
    self.is_loading_setting_ui = True
    if not os.path.exists(f"{self.config_folder}/settings.json"):
        return

    if just_load_value:
        try:
            loading_config = json.load(open(f"{self.config_folder}/settings.json"))
            # We merge the two config, if we add a value later and the user don't have it.
            self.config = merged_dict = {**self.config, **loading_config}
            log.debug("Loaded first run settings.")
            return
        except Exception as e:
            self.config = self.config
            log.warning("Could not load default config. Falling back to default.")
            return
            
    try:
        loading_config = json.load(open(f"{self.config_folder}/settings.json"))

        # Set the language radio
        self.action_show_changeLang.set_state(GLib.Variant('s', loading_config["set_langue_name"]))

        # Load the default value in the UI if the name of the saved language doesn't match the current language at this position.
        if str(self.action_show_changeLang.get_state()).replace("'", "") != loading_config["set_langue_name"]:
            log.info("Error: saved language doesn't match the UI, showing the default. Also, getting a new word")
            self.action_show_changeLang.set_state(GLib.Variant('s', 'Français'))
            game.new_word(self=self)

        self.config = loading_config
        log.info("Loaded modified settings.")

    except Exception as error:
        log.warning(error)
        log.warning("Error in config file... Using default values")
    self.is_loading_setting_ui = False
