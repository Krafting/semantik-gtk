import gi 
import numpy as np
import logging as log
import os
import webbrowser

gi.require_version('Gtk', '4.0')
gi.require_version('Pango', '1.0')
gi.require_version('Adw', '1')
from gi.repository import Gtk, Adw, Gio, Gdk, Graphene, GLib, GObject, Pango

from utils.ui.settings import *

def open_browser(button, action, link, self=False) -> None:
    """
    Function to open link in the browser
    """
    webbrowser.open(str(link))

def list_dir(dirname):
    """
    Function to get an array of all the folder in a specific folder
    """
    subfolders = [f.name for f in os.scandir(dirname) if f.is_dir()]
    return subfolders

def copy_stuff(event, data, self = False):
    """
    Function to copy stuff in the clipboard
    """
    self.clipboard = Gdk.Display().get_default().get_clipboard()
    data_value = Gdk.ContentProvider().new_for_value(value=data)
    self.clipboard.set_content(data_value)

def is_valid_word(word):
    """
    Function to verify if it's a valid word
    """
    if word == "":
        return False
    elif "!" in word:
        return False
    else:
        return True

def sort_guess_array(array=[]):
    """
    Function to sort the array of all the guess to sort them correclty and then get the correct values
    """
    new_array = sorted(array, key=lambda x: x["score"], reverse=True)
    return new_array

def get_score_emoji(self, score):
    """
    Function to get the emoji from a given word score
    """
    emoji = "🟥"
    if score < 0:
        emoji = "🧊"
    if score >= 0:
        emoji = "🥶"
    if score >= self.most_similar_1000 * 100:
        emoji = "😎"
    if score >= self.most_similar_100 * 100:
        emoji = "🥵"
    if score >= self.most_similar_10 * 100:
        emoji = "🔥"
    if score >= self.most_similar_1 * 100:
        emoji = "😱"
    if score >= 100:
        emoji = "🥳"

    if score <= -999:
        emoji = "🟦"

    return emoji

def get_pourmille_from_score(self, score):
    """
    Function to get the %0 from the score.
    """
    points_x = [
        -100,
        0,
        float(self.most_similar_1000) * 100,
        float(self.most_similar_100) * 100,
        float(self.most_similar_10) * 100,
        float(self.most_similar_1) * 100,
        100,
    ]
    points_y = [-100, 0, 1, 900, 990, 999, 1000]

    pourmillage = np.interp(score, points_x, points_y)

    if pourmillage < 1:
        pourmillage = 0

    return int(pourmillage)

def open_store(button, action, self):
    """
    Option to hopefully open the App Store of any distro with a search term like "SementiK Lang"
    """
    uri = "appstream://net.krafting.SemantiK"
    Gio.AppInfo.launch_default_for_uri(uri, None)


def change_language(button, action, self):
    """
    Function called when changing the language
    We update the UI selection and call for save_setting which call for changing the language
    """
    if action:
        self.action_show_changeLang.set_state(action)
        save_settings(False, False, self=self)
