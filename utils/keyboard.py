import gi 
import logging as log

gi.require_version('Gtk', '4.0')
gi.require_version('Pango', '1.0')
gi.require_version('Adw', '1')
from gi.repository import Gtk, Adw, Gio, Gdk, Graphene, GLib, GObject, Pango

from utils.ui.shortcuts import *
from utils.ui.settings import *
from utils.ui.sharing import *
from utils.ui.indice import *
from utils.game import *

def key_press(controller, keyval, keycode, state, self=False, is_main_window = True) -> None:
    """
    Function to do stuff when keyboard input is pressed.
    Used mostly for Shortcuts
    """
    log.debug(f"Key pressed: {keyval}")

    modifiers = state & Gtk.accelerator_get_default_mod_mask()
    control_mask = Gdk.ModifierType.CONTROL_MASK
    shift_mask = Gdk.ModifierType.SHIFT_MASK
    alt_mask = Gdk.ModifierType.ALT_MASK
    shift_ctrl_mask = control_mask | shift_mask

    # Ctrl+<KEY>
    if control_mask == modifiers:
        # Show new record window on Ctrl + N
        if keyval == Gdk.KEY_n:
            show_sharing(self=self)
        if keyval == Gdk.KEY_q:
            self.window.close()
        if keyval == Gdk.KEY_i:
            show_indice(self=self)
        if keyval == Gdk.KEY_question:
            show_shortcuts(self=self)
    # Ctrl+Shift+<KEY>
    elif modifiers == shift_ctrl_mask:
        # Duplicate for keyboards that needs to press SHIFT to type the character
        if keyval == Gdk.KEY_question:
            show_shortcuts(self=self)
    # No modifier
    else:
        log.debug('None')


def input_event_handle(controller = "", keyval = "", keycode = "", state = "", self = False):
    """
    Function when using modifiers key in the input text box, used to show history of words
    """
    if keyval == Gdk.KEY_Up or keyval == Gdk.KEY_KP_Up:
        self.current_history_pos = self.current_history_pos - 1
        if self.current_history_pos < -len(self.latest_game["guessed_word_unordered"]):
            self.current_history_pos = -len(self.latest_game["guessed_word_unordered"])
            return
        try:
            self.WordGuess_Input.set_text(self.latest_game["guessed_word_unordered"][self.current_history_pos])
            # Set the caret position
            num_char_input = list(self.WordGuess_Input.get_text())
            self.WordGuess_Input.set_position(len(num_char_input))

        except:
            log.error("Could not load history (ARROW UP)")

    if keyval == Gdk.KEY_Down or keyval == Gdk.KEY_KP_Down:
        self.current_history_pos = self.current_history_pos + 1
        if self.current_history_pos >= 0:
            self.current_history_pos = 0
            self.WordGuess_Input.set_text("")
            return
        try:
            self.WordGuess_Input.set_text(self.latest_game["guessed_word_unordered"][self.current_history_pos])
            # Set the caret position
            num_char_input = list(self.WordGuess_Input.get_text())
            self.WordGuess_Input.set_position(len(num_char_input))
        except:
            log.error("Could not load history (ARROW DOWN)")